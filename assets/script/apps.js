let lastAppListHash = "?";

const load_apps = fetch(document.currentScript.dataset.appApi)
  .then(response => {
    return response.json();
  });

const load_apps_en = fetch(document.currentScript.dataset.appApiEn)
  .then(response => {
    return response.json();
});

// Initialize search with random results
search('');

async function search(name, proglang) {
  const search = name.toLocaleLowerCase().trim();

  const api = await load_apps;
  const apps = Object.values(api.apps);
  const api_en = await load_apps_en;

  let random = false;

  if (search || proglang) {
    found = apps;
    if (proglang) {
      found = found.filter(app => app.programming_languages.includes(proglang));
    }
    if (search) {
      // Search in current language and English
      found = found.filter(app => name_matches(search, app) || name_matches(search, api_en.apps[app.id])).slice(0, 8);
    }
    // Show more if only searching for programming language
    if (found.length > 32) {
      found = found.slice(0, 32);
    }
  } else {
    random = true;
    found = apps
      .map(value => ({ value, sort: Math.random() }))
      .sort((x, y) => x.sort - y.sort)
      .slice(0, 8)
      .map(({ value }) => value);
  }

  appListHash = found.map(data => data.id).reduce((x, y) => x + y, "");

  if (lastAppListHash == appListHash) {
    return;
  }

  lastAppListHash = appListHash;

  const template = document.getElementById('app-result');
  let list = [];

  for (const app of found) {
    const row = template.content.cloneNode(true);

    row.querySelector(".app-name").textContent = app.name;
    row.querySelector("a").href += `app/${app.page_id}/`
    row.querySelector("img.app-icon").src = `https://apps.gnome.org/icons/scalable/${app.id}.svg`;

    list.push(row);
  }

  const div = document.getElementById('app-results');

  if (list.length === 0) {
    div.dataset.status = "empty";
  } else if (random) {
    div.dataset.status = "random";
  } else {
    div.dataset.status = "search-result";
  }

  document.getElementById('app-results-list').replaceChildren(...list);
}

function name_matches(search, app) {
  if (app.name.toLocaleLowerCase().includes(search)) {
    return true;
  }

  if (app.page_id.toLocaleLowerCase().includes(search)) {
    return true;
  }

  if (app.summary.toLocaleLowerCase().includes(search)) {
    return true;
  }

  return false;
}

document.addEventListener("DOMContentLoaded", function (event) {
  const col = new Intl.Collator('en', { sensitivity: 'base' });
  const name = document.getElementById("app-search-name");
  const proglang = document.getElementById("app-search-proglang");

  name.addEventListener('input', event => search(name.value, proglang.value));
  proglang.addEventListener('input', event => search(name.value, proglang.value));
});
