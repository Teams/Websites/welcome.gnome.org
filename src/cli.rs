use crate::generate;
use structopt::StructOpt;

/// A basic example
#[derive(StructOpt, Debug)]
pub struct CliOpt {
    #[structopt(long)]
    /// Regenerate .pot-files (translation strings)
    pub regen_pot: bool,
}

pub fn args() -> CliOpt {
    CliOpt::from_args()
}

pub fn main() {
    let cli_opt = args();

    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info"),
    );

    color_eyre::install().unwrap();

    generate::generate().unwrap();

    if cli_opt.regen_pot {
        page_tools::i18n::write_pot_file("po/welcome.gnome.org.pot").unwrap();
    }
}
