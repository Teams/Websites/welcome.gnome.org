use askama::Template;
use page_tools::{Apps, Site};

use std::ops::Deref;

// used in templates
use crate::filters;

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index<'a> {
    pub site: &'a Site,
    pub content: String,
}

#[derive(Template)]
#[template(path = "index.md")]
pub struct IndexContent<'a> {
    pub site: &'a Site,
}

#[derive(Template)]
#[template(path = "app.html")]
pub struct App<'a> {
    pub app: &'a page_tools::App,
    pub site: &'a Site,
    pub content: String,
}

#[derive(Template)]
#[template(path = "app.md")]
pub struct AppContent<'a> {
    pub app: &'a page_tools::App,
    pub site: &'a Site,
    pub apps: &'a Apps,
}

// Teams

#[derive(Template)]
#[template(path = "team/design.html")]
pub struct Design<'a> {
    pub site: &'a Site,
    pub content: String,
}

#[derive(Template)]
#[template(path = "team/design.md")]
pub struct DesignContent {}

#[derive(Template)]
#[template(path = "team/governance.html")]
pub struct Governance<'a> {
    pub site: &'a Site,
    pub content: String,
}

#[derive(Template)]
#[template(path = "team/governance.md")]
pub struct GovernanceContent {}

#[derive(Template)]
#[template(path = "team/programming.html")]
pub struct Programming<'a> {
    pub site: &'a Site,
    pub content: String,
}

#[derive(Template)]
#[template(path = "team/programming.md")]
pub struct ProgrammingContent<'a> {
    pub site: &'a Site,
    pub apps: &'a Apps,
}

#[derive(Template)]
#[template(path = "team/websites.html")]
pub struct Websites<'a> {
    pub site: &'a Site,
    pub content: String,
}

#[derive(Template)]
#[template(path = "team/websites.md")]
pub struct WebsitesContent<'a> {
    pub site: &'a Site,
    pub apps: &'a Apps,
}

#[derive(Template)]
#[template(path = "team/engagement.html")]
pub struct Engagement<'a> {
    pub site: &'a Site,
    pub content: String,
}

#[derive(Template)]
#[template(path = "team/engagement.md")]
pub struct EngagementContent {}
