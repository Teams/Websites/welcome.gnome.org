mod cli;
mod filters;
mod generate;
mod page;

#[macro_use]
extern crate log;

fn main() {
    cli::main();
}
