# Welcome to GNOME

Welcome to GNOME is a landing site for people wanting to contribute in whatever way to the GNOME project.

This site provides detail pages with automatically generated information for all Core and Circle apps.

- https://welcome.gnome.org

## Contributing

- The content can mostly be found in the `.md` files in `templates/`.
- Some more settings and the page structure are contained in the `.html` files.
- The `scss/` files are automatically build by the rust code.
- The page is statically generated into `public/`.
- Content specific to apps is generated from the [apps.gnome.org API](https://gitlab.gnome.org/World/apps-for-gnome#api).

## Building the Page

### Builder

The easiest way is to clone the repository via [Builder](https://apps.gnome.org/app/org.gnome.Builder/) and press the “Run” button. The page will generated and a web server started afterwards. The page is available via `http://localhost:8000/`. This might not work with Builder 45. The bug has been fixed in the [nightly version](https://welcome.gnome.org/app/Builder/#installing-a-nightly-build).

The app data are cached. To download updated app data, just delete the `cache` directory. This can fix errors if the API has an incompatible format change.

### Manual

Calling

```
meson builddir
ninja install -C builddir
cargo run
```

builds the page into `public/`. You can serve it locally via

```
python3 -m http.server -d public/
```

## Project Mission

The main goal of Welcome to GNOME is to create a welcoming atmosphere and show people that they can get started with contributing to GNOME.

It is however not a goal to create detailled step-for-step instructions, necessary for a first contribution. Neither is this feasible, since problem solving will almost always require own research initiative, nor is it the goal of this project to replace existing resources like Team pages, the Handbook, or the Development Center. Rather, it is a resource for getting guided to the most important resources within the project for respective contribution steps and tasks.

1. Create a welcoming atmosphere.

- Welcome people instead of categorizing them as "newcomers."
- No technical terms on the start page.
- Make the pages visually appealing.

2. Keep the entry barrier low to show people that they can get going.

- Translate the pages that contain the first steps.
- Automatically link to the relevant pages like the respective translation page for an app or to the documentation for the programming language the app uses.
- Provide the correct steps for either GitLab or GitHub on app pages with the respective links.

3. Provide a page to look up basic auto-generated information.

- Lookup apps written in a specific programming language to see how they solved a problem.
- Look up how an app can be installed (Flathub, Nightly).