{% import "_macros.html" as macro %}

## Welcome to the Engagement Team!

This page will help you get started contributing as part of the Engagement team to help us promote GNOME and make it an even cooler project to be part of. All skill sets are welcome!

## Quick Start Guide

Try doing the following things in order to quickly get started.

### Join gnome.element.io

Our **Matrix Channel** is the main place to chat with us and get involved in the Engagement Team. Sign up for the [#engagement:gnome.org](https://matrix.to/#/#engagement:gnome.org) channel to ask any questions you have or discuss how to make GNOME even more awesome. Don’t be afraid to just say hi!

<ul class="links">
    {% call macro::link("chat", "https://matrix.to/#/#engagement:gnome.org", "#engagement:gnome.org"|t, "Join the Matrix channel of the Engagement Team"|t) %}
</ul>

### Find a task on our GitLab Board

Access our **GitLab** board and contribute, collaborate, and help us to improve our work by contributing to tasks and projects that we’re currently handling -- or why not suggest new tasks and projects? Message one of the project ["masters"](https://gitlab.gnome.org/Teams/Engagement/General/-/project_members) to get added as a developer.

<ul class="links">
    {% call macro::link("issues", "https://gitlab.gnome.org/Teams/Engagement/General", "Engagement Team on GitLab"|t, "Find a task on our GitLab Board or suggest new tasks and projects"|t) %}
</ul>

### Sign up for the GNOME Wiki

Due to spam problems, we had to lock down the **GNOME Wiki**. Since we use this wiki for a lot of our projects, you should sign up for an account and then get someone to add you to our [trusted editors page](https://wiki.gnome.org/TrustedEditorGroup) so that you can start editing the wiki on your own.

<ul class="links">
    {% call macro::link("docs", "https://wiki.gnome.org/action/newaccount/TrustedEditorGroup?action=newaccount", "Create Wiki Account"|t, "Create an account on the GNOME Wiki"|t) %}
</ul>

## Next Steps & Further Reading

Once you have followed the quick start guide above, these are some good things to do to continue getting started:
- **Join a team meeting.** Look out for details of our next team meeting on [events.gnome.org](https://events.gnome.org/category/5/), and come along.
- **Find an interest.** Check out the list of some of our activities on [the main Engagement Team page](https://wiki.gnome.org/Engagement). There are all sorts of things to do, from designing t-shirts to web design and development to coordinating our annual reports.
- **Get familiar with our brand.** GNOME is all about being simple and elegant. Read our [brand guidelines](https://wiki.gnome.org/Engagement/BrandGuidelines) to familiarize yourself with our design ethos, especially if you’ll be helping us with design.
- **Find a mentor.** [Contact an Engagement Team mentor](https://wiki.gnome.org/Newcomers/Mentors#Design.2C_Documentation.2C_Engagement.2C_etc.), to get advice or just say hi.
- **Use GNOME.** If you don’t already, you should try and use GNOME 3. See the [getting GNOME page](https://www.gnome.org/getting-gnome/) for information on how to get started.
- **Learn about the GNOME project.** Read our [about page](http://www.gnome.org/about/) and the 20 year anniversary [website](http://www.happybirthdaygnome.org/). The [newcomers project tour](https://wiki.gnome.org/Engagement/Newcomers/ProjectTour) also points out some of the main parts of the project.
- **Follow our daily goings on.** Read [Planet GNOME](http://planet.gnome.org/), and follow us on social media.

