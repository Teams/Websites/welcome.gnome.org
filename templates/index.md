{% import "_macros.html" as macro %}

## Our community

A very warm welcome to the GNOME project. We want everyone to have an enjoyable and fulfilling experience. For this, the GNOME community prioritizes marginalized people’s safety over privileged people’s comfort. To learn more about how you can find support and what is expected from contributors, please have a look at our [GNOME Code of Conduct](https://conduct.gnome.org).

<ul class="links">
  {% call macro::link("docs", "https://conduct.gnome.org", "GNOME Code of Conduct"|t, "All participants are expected to follow this Code of Conduct"|t) %}
</ul>

In GNOME we use Matrix chat to hang out together and discuss projects. You can get started in the [#newcomers room](https://matrix.to/#/#newcomers:gnome.org) or directly reach out in a more specific chat room.

<ul class="links">
  {% call macro::link("chat", "https://matrix.to/#/#newcomers:gnome.org", "#newcomers:gnome.org", "The “GNOME Newcomers” chat can help you get started"|t) %}
  {% call macro::link("chat", "https://matrix.to/#/#community:gnome.org", "#community:gnome.org", "Explore all the GNOME-related chat rooms in our Matrix space"|t) %}
</ul>

</div>
</section>
<section>
<div>

## Where to contribute

Within GNOME there are many areas where you can contribute and a plethora of skills that are appreciated.

<div class="categories">
  <a href="team/programming/">
    <h4>{{ "Programming"|t }}</h4>
    <p>{{ "Write code and engineer technical solutions"|t }}</p>
  </a>

  <a href="team/design/">
    <h4>{{ "Design"|t }}</h4>
    <p>{{ "Design user interfaces, user experience, and visuals"|t }}</p>
  </a>
  {#
  <a href="team/engagement/">
    <h4>{{ "Engagement"|t }}</h4>
    <p>{{ "Work on marketing, user outreach, and events organization"|t }}</p>
  </a>
  #}
  <a href="https://wiki.gnome.org/TranslationProject">
    <h4>{{ "Translation"|t }}</h4>
    <p>{{ "Help to translate software and websites into your language"|t }}</p>
  </a>
  <a href="https://wiki.gnome.org/DocumentationProject/Contributing">
    <h4>{{ "Documentation"|t }}</h4>
    <p>{{ "Write documentation, help pages, and manuals"|t }}</p>
  </a>
  <a href="team/websites/">
    <h4>{{ "Websites"|t }}</h4>
    <p>{{ "Maintain websites and create content"|t }}</p>
  </a>
  {#
  <a href="team/governance/">
    <h4>{{ "Governance"|t }}</h4>
    <p>{{ "Guide the foundation’s direction and raise funding"|t }}</p>
  </a>
  #}
</div>

<p><a href="https://wiki.gnome.org/Teams" class="pill">{{ "View all GNOME Teams"|t }}</a></p>

</div>
</section>
<section>
<div>

## Contribute to an app

Below, you can find specific contribution guides for all GNOME apps. If you are not sure where to contribute, consider choosing an app that you use yourself. Try to solve something that is important to you or where you feel an inner motivation.

{% include "_app-search.html" %}

</div>
</section>
<section>
<div>

## Internships

You can begin or continue your journey with GNOME with a remote internship including mentoring and a stipend. You can both choose from suggested projects or suggest your own.

<div class="internships">
  <a href="https://wiki.gnome.org/Outreach/Outreachy">
    <h3>{{ "Outreachy"|t }}</h3>
    <p>{{ "Outreachy provides internships to people subject to systemic bias and impacted by underrepresentation in the technical industry where they are living."|t }}</p>
  </a>
  <a href="https://gsoc.gnome.org/">
    <h3>{{ "Google Summer of Code"|t }}</h3>
    <p>{{ "GSoC is an online mentoring program focused on introducing new contributors to open source software development."|t }}</p>
  </a>
</div>

## Something missing?

The *Welcome to GNOME* pages are new. [Let us know](https://gitlab.gnome.org/Teams/Websites/welcome.gnome.org/-/issues) if you couldn’t find what you were hoping to find on this website.
