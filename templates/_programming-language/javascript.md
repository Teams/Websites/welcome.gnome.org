We recommend the [GJS Developer Guide](https://gjs.guide/guides/) to get started with JavaScript in GNOME. You can find other important resources below.

<ul class="links">
  {% call macro::link("docs", "https://gjs-docs.gnome.org", "JavaScript Bindings Reference"|t, "How to use GTK, the underlying library for user interfaces, and other libraries"|t) %}
  {% call macro::link("chat", "https://matrix.to/#/#javascript:gnome.org", "#javascript:gnome.org", "Matrix channel for all questions related to JavaScript in GNOME"|t) %}
</ul>
