If you are familiar with the basics of the [Rust language](https://www.rust-lang.org), we recommend the online book [GUI development with Rust and GTK&nbsp;4](https://gtk-rs.org/gtk4-rs/stable/latest/book/). This book explains how Rust can be used to create apps in the GNOME ecosystem. You can find other important resources below.

<ul class="links">
  {% call macro::link("docs", "https://gtk-rs.org/gtk4-rs/stable/latest/docs/gtk4/", "Rust GTK&nbsp;4 docs"|t, "GTK is the underlying library for the user interface"|t) %}
  {% call macro::link("docs", "https://world.pages.gitlab.gnome.org/Rust/libadwaita-rs/stable/latest/docs/libadwaita", "Rust libadwaita docs"|t, "Building blocks for modern GNOME apps"|t) %}
  {% call macro::link("docs", "https://gtk-rs.org/gtk-rs-core/stable/latest/docs/gio/", "Rust GTK Core docs"|t, "GTK is built on libraries like GLib and GIO"|t) %}
  {% call macro::link("chat", "https://matrix.to/#/#rust:gnome.org", "#rust:gnome.org", "The “Rust ❤️ GNOME” Matrix channel: All about using Rust in GNOME"|t) %}
</ul>
